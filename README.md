# ob-jira-rest.el

An extension to restclient.el that provides org-babel support for Jira JQL Queries.

## Installation

First install restclient.el with your package manager:

    M-x package-install restclient

Install ob-jira-rest.el somewhere in your `load-path`

    ;; Example For emacs prelude
    (load (concat prelude-vendor-dir "/ob-restclient.el"))

OR place in your `load-path` and load with org-babel

    (org-babel-do-load-languages
      'org-babel-load-languages
      '((ob-jira-rest . t)))


## Configuration

Authentication has to be stored somewhere. If can either be in the org-babel header like this:

    #+BEGIN_SRC jira-rest :user "yourjiralogin" :password "yourjirapassword"

The better way is to create a file in your home directory `~/.jira-auth.el` with the contents:

    (setq jira-auth '((:user . "USER") (:password . "PASSWORD")))

The org-babel block supports the following options:

* `:max-results` maximum results to be returned from the Jira API (DEFAULT: 100)
* `:formatter`   see section on formatting results below (DEFAULT: list-format)
* `:user`        the user that you use to log into Jira (REQUIRED if not using jira-auth.el)
* `:password`    the password that you use to log into Jira (REQUIRED if not using jira-auth.el)
* `:rest-url`    The REST Url for your Jira rest search endpoint (REQUIRED)


## Usage

Create an org-babel block for `jira-rest` with your desired options.
For example, get all of your assigned issues:

    #+BEGIN_SRC jira-rest
    assignee = currentUser() AND resolution = Unresolved ORDER BY updatedDate DESC
    #+END_SRC

Any valid JQL expression will be used to search Jira and return a list of issues and their summaries
When a valid JQL block is entered, type `C-c` to execute the query.

## Formatters:

The default formatter is `list-format`. Other available formatters are:

* `:list-format-with-tabs` Tabs between the issue key and summary. Makes it easy to convert to a table
* `:org-table-format` Outputs Issue and summary in an org-table.

## Author

Jason Stewart and Fusionary Media
